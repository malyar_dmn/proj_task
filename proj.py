import random
import datetime

class StandartProject:
    def __init__(self, price, type, time):
        self.price = price
        self.type = type
        self.time = time

    id = random.randint(1, 20)
    deadline = None

class TenDaysProject(StandartProject):
    def __init__(self, *args):
        super().__init__(*args)
        self.price = self.price + (self.price * 0.6)


class InvestorsProject(StandartProject):
    def __init__(self, *args):
        super().__init__(*args)
        self.price = self.price - (self.price * 0.2)




class ProjectManager:
    def __init__(self):
        self.projectsList = []

    def createProject(self):
        while True:
            inpType = input('Enter project type: ')
            inpPrice = int(input('Enter project price: '))
            inpTime = input('Enter a project deadline in YYYY-MM-DD format: ')
            manager.projectDeadline()
            if(inpType == 'standart'):
                self.projectsList.append(StandartProject(inpPrice, inpType, inpTime))
            elif(inpType == 'ten days'):
                self.projectsList.append(TenDaysProject(inpPrice, inpType, inpTime))
            elif(inpType == 'investors'):
                self.projectsList.append(InvestorsProject(inpPrice, inpType, inpTime))
            else:
                raise ValueError('Wrong value')

            operation = input('Type "cancel" if you want to cancel the operation\n'
                              'Type "list" if you want examine the list of projects\n')
            if(operation == 'cancel'):
                break
            elif(operation == 'list'):
                manager.getProjectList()
                break



    def getProjectList(self):
        for proj in self.projectsList:
            print(proj.type, proj.price)



manager = ProjectManager()
manager.createProject()
# manager.getProjectList()


# inp = input('Enter a date in YYYY-MM-DD format\n')
# year, month, day = map(int, inp.split('-'))
# d = datetime.date(year, month, day)
#
# now = datetime.date.today()
# delta = d - now
# print(delta)
